<?php
/**
 * Plugin Name: TTN Eguerdiko Ahotsa
 * Description: Azken Eguerdiko Ahotsa irratsaioak
 * Version: 0.0.6
 * Author: Jimakker
 * Author URI: http://twitter.com/jimakker
 */


add_action( 'widgets_init', 'ttn_eguerdikoahotsa' );


function ttn_eguerdikoahotsa() {
	register_widget( 'Ttn_Eguerdikoahotsa' );
}

class Ttn_Eguerdikoahotsa extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'ttn-eguerdikoahotsa', 'description' => __('Azken Eguerdiko Ahotsa irratsaioak', 'ttn-eguerdikoahotsa') );

		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ttn-eguerdikoahotsa-widget' );

		parent::__construct( 'ttn-eguerdikoahotsa-widget', __('TTN Eguerdiko Ahotsa', 'ttn-eguerdikoahotsa'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		//Our variables from the widget settings.

		echo $before_widget;


        ttn_eguerdikoahotsa_output_fn();

		?>




		<?php
		echo $after_widget;
	}

	//Update the widget

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;


		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('TTN Eguerdiko Ahotsa', 'example'), 'limit' => __('10', 'example'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>



	<?php
	}




}



function ttn_eguerdikoahotsa_output_fn() {
        ?>
        <div class="panel panel-default" style="border-radius:0;">

		  <div class="panel-heading" style="background-image:url(<?php plugins_url( 'assets/img/eguerdikoahotsa_banner.png', __FILE__ );?>);height:50px;border-radius:0;border-color:#333;display:block;background-repeat:no-repeat;background-size:auto 100%;background-color:#fff;background-position:left;">
                <a href="/irratsaioak/eguerdiko-ahotsa/" title="Eguerdiko Ahotsa" style="display:block;width:100%;height:100%;"></a>
            </div>

			<div class="list-group">
			<?php
			$args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'cat' => 896, 'category__not_in'=>9 );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();
			?>
				<a title="<?php echo get_the_title();?>" href="<?php echo get_permalink();?>" class="list-group-item" style="border-radius:0;color:#fff;background-color:#555;border-color:#333;">
					<?php the_title();?>
				</a>

				<?php
			endwhile;
			?>
			</div>
		</div>
    <?php
    }






?>
